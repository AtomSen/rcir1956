package rcir1956MV.controller;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import rcir1956MV.exception.DuplicateIntrebareException;
import rcir1956MV.exception.InputValidationFailedException;
import rcir1956MV.exception.NotAbleToCreateStatisticsException;
import rcir1956MV.exception.NotAbleToCreateTestException;
import rcir1956MV.repository.IntrebariRepository;

import java.io.File;

import static org.junit.Assert.*;

public class AppControllerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private IntrebariRepository repo;
    private AppController ctr;

    @Before
    public void setUp() throws Exception {
        File f = new File("teste.txt");
        f.delete();
        repo = new IntrebariRepository("teste.txt");
        ctr = new AppController(repo);
    }

    @Test
    public void Req02_TC01() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        thrown.expect(NotAbleToCreateTestException.class);
        thrown.expectMessage("Nu exista suficiente" +
                " intrebari pentru crearea unui test!(5)");
        ctr.createNewTest();
    }
    @Test
    public void Req02_TC02() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        thrown.expect(NotAbleToCreateTestException.class);
        thrown.expectMessage("Nu exista suficiente domenii pentru crearea unui test!(5)");
        ctr.addNewIntrebare("enunt", "varianta1", "varianta2", "varianta3", 2, "domeniu");
        ctr.addNewIntrebare("enunt1", "varianta1", "varianta2", "varianta3", 2, "domeniu");
        ctr.addNewIntrebare("enunt2", "varianta1", "varianta2", "varianta3", 2, "domeniu");
        ctr.addNewIntrebare("enunt3", "varianta1", "varianta2", "varianta3", 2, "domeniu");
        ctr.addNewIntrebare("enunt4", "varianta1", "varianta2", "varianta3", 2, "domeniu");
        ctr.createNewTest();
    }
    @Test
    public void Req02_TC03() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        ctr.addNewIntrebare("enunt", "varianta1", "varianta2", "varianta3", 2, "domeniu");
        ctr.addNewIntrebare("enunt1", "varianta1", "varianta2", "varianta3", 2, "domeniu1");
        ctr.addNewIntrebare("enunt2", "varianta1", "varianta2", "varianta3", 2, "domeniu2");
        ctr.addNewIntrebare("enunt3", "varianta1", "varianta2", "varianta3", 2, "domeniu3");
        ctr.addNewIntrebare("enunt4", "varianta1", "varianta2", "varianta3", 2, "domeniu4");
        assertEquals(5,ctr.createNewTest().size());
    }
    @Test
    public void Req02_TC04() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        ctr.addNewIntrebare("enunt", "varianta1", "varianta2", "varianta3", 2, "domeniu");
        ctr.addNewIntrebare("enunt1", "varianta1", "varianta2", "varianta3", 2, "domeniu1");
        ctr.addNewIntrebare("enunt2", "varianta1", "varianta2", "varianta3", 2, "domeniu2");
        ctr.addNewIntrebare("enunt3", "varianta1", "varianta2", "varianta3", 2, "domeniu3");
        ctr.addNewIntrebare("enunt4", "varianta1", "varianta2", "varianta3", 2, "domeniu2");
        ctr.addNewIntrebare("enunt4", "varianta1", "varianta2", "varianta3", 2, "domeniu3");
        ctr.addNewIntrebare("enunt2", "varianta1", "varianta2", "varianta3", 2, "domeniu4");
        assertEquals(5,ctr.createNewTest().size());
    }
    @Test
    public void Req02_TC05() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        ctr.addNewIntrebare("enunt", "varianta1", "varianta2", "varianta3", 2, "domeniu1");
        ctr.addNewIntrebare("enunt1", "varianta1", "varianta2", "varianta3", 2, "domeniu1");
        ctr.addNewIntrebare("enunt2", "varianta1", "varianta2", "varianta3", 2, "domeniu4");
        ctr.addNewIntrebare("enunt3", "varianta1", "varianta2", "varianta3", 2, "domeniu3");
        ctr.addNewIntrebare("enunt4", "varianta1", "varianta2", "varianta3", 2, "domeniu2");
        ctr.addNewIntrebare("enunt2", "varianta1", "varianta2", "varianta3", 2, "domeniu5");
        assertEquals(5,ctr.createNewTest().size());
    }

    @Test
    public void Req03_TC01() throws NotAbleToCreateStatisticsException {
        thrown.expect(NotAbleToCreateStatisticsException.class);
        thrown.expectMessage("Repository-ul nu contine nicio intrebare!");
        ctr.getStatistica();
    }
    @Test
    public void Req03_TC02() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateStatisticsException {
        ctr.addNewIntrebare("enunt", "varianta1", "varianta2", "varianta3", 2, "domeniu1");
        ctr.addNewIntrebare("enunt1", "varianta1", "varianta2", "varianta3", 2, "domeniu1");
        assertNotEquals(null,ctr.getStatistica());
    }
}