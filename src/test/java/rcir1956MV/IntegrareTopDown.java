package rcir1956MV;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import rcir1956MV.controller.AppController;
import rcir1956MV.exception.DuplicateIntrebareException;
import rcir1956MV.exception.InputValidationFailedException;
import rcir1956MV.exception.NotAbleToCreateStatisticsException;
import rcir1956MV.exception.NotAbleToCreateTestException;
import rcir1956MV.repository.IntrebariRepository;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class IntegrareTopDown {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private IntrebariRepository repo;
    private AppController ctr;

    @Before
    public void setUp() throws Exception {
        File f = new File("teste.txt");
        f.delete();
        repo = new IntrebariRepository("teste.txt");
        ctr = new AppController(repo);
    }
    @Test
    public void TC1_ECP() throws DuplicateIntrebareException, InputValidationFailedException {
        boolean rez = repo.addIntrebare("enunt", "varianta1", "varianta2", "varianta3", 2, "domeniu");
        assertTrue("should be true, it is:" + rez, rez);
    }
    @Test
    public void Req02_TC01() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        ctr.addNewIntrebare("enunt", "varianta1", "varianta2", "varianta3", 2, "domeniu");
        ctr.addNewIntrebare("enunt1", "varianta1", "varianta2", "varianta3", 2, "domeniu1");
        ctr.addNewIntrebare("enunt2", "varianta3", "varianta2", "varianta3", 2, "domeniu2");
        ctr.addNewIntrebare("enunt3", "varianta1", "varianta2", "varianta3", 2, "domeniu3");
        ctr.addNewIntrebare("enunt4", "varianta1", "varianta2", "varianta3", 2, "domeniu4");
        assertEquals(5,ctr.createNewTest().size());
    }
    @Test
    public void integrationTopDown() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateStatisticsException, NotAbleToCreateTestException {
        boolean rez;
        rez=repo.addIntrebare("enunt", "varianta1", "varianta2", "varianta3", 2, "domeniu");
        assertTrue("should be true, it is:" + rez, rez);
        rez=repo.addIntrebare("enunt1", "varianta1", "varianta2", "varianta3", 2, "domeniu1");
        assertTrue("should be true, it is:" + rez, rez);
        rez=repo.addIntrebare("enunt2", "varianta1", "varianta2", "varianta3", 2, "domeniu2");
        assertTrue("should be true, it is:" + rez, rez);
        rez=repo.addIntrebare("enunt3", "varianta3", "varianta2", "varianta3", 2, "domeniu3");
        assertTrue("should be true, it is:" + rez, rez);
        rez=repo.addIntrebare("enunt4", "varianta1", "varianta2", "varianta3", 2, "domeniu4");
        assertTrue("should be true, it is:" + rez, rez);
        assertEquals(5,ctr.createNewTest().size());
        assertNotEquals(null,ctr.getStatistica());
    }
}
